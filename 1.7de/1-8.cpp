#include <iostream>
#include <cmath>

using namespace std;

const int ROW = 5;
const int COL = 5;
int matrix[ROW][COL];

void numbersCheck() {
    float first, second;
    cout << "First number: ";
    cin >> first;
    cout << "Second number: ";
    cin >> second;
    if(first > second) {
      cout << "First > second\n";
    } else if(second > first) {
      cout << "Second > first\n";
    } else {
      cout << "Equals\n";
    }
}

void yearCheck() {
   int year;
   cout << "Input year: ";
   cin >> year;
   if (((year % 4 == 0) && (year % 100 != 0)) || (year % 400 == 0)) {
       cout << "High year";
   } else {
       cout << "Common year";
   }
}

void squareSolution() {
  double a,b,c,d,x1,x2;
 
  cout << "Input a: ";
  cin >> a;
  cout << "Input b: ";
  cin >> b;
  cout << "Input c: ";
  cin >> c;
 
  d = b * b - 4 * a * c;
  if(d > 0) 
  {
    x1 = ((-b) + sqrt(d)) / (2 * a);
    x2 = ((-b) - sqrt(d)) / (2 * a);
    cout << "x1 = " << x1 << "\n";
    cout << "x2 = " << x2 << "\n";
  }
  if(d == 0)
  {
    x1 = -(b / (2 * a));
    cout << "x1 = x2 = " << x1 << "\n";
  }
  if(d < 0)
    cout << "D < 0";    
}

void checkEven() {
    int num;
    cout << "Input number: ";
    cin >> num;
    if(num % 2 == 0) {
        cout << "Even\n";
    } else {
        cout << "Odd\n";
    }
}

void squares() {
    for(int i = 1; i<=10; i++) {
        cout << i << "^2: " << pow(i, 2) << "\n";
    }
}

void getMax() {
    int x = 0;
    int max = 0 ;
    
    cout << "Enter value (0 for exit): \n";
    while(true)
    {
        cout << "[root@localhost]# ";
        cin >> x;
        if(x == 0)
            break;
        if(x > max)
            max = x;
    }
    cout << "Max is: " << max << "\n";
}

void stepFunction() {
   //y=-2 * x^2 - 5 * x - 8
   double start=-4.0, stop=4.0;
   double x = start;
   while(x <= stop)
   {
    cout << x << ": " << (-2 * pow(x, 2) - 5 * x - 8) << "\n";
    x += 0.5;
   }
}

void generateArray() {
    srand(time(NULL));
    for (int i = 0; i < ROW; i++) {
        for (int j = 0; j < COL; j++) {
            matrix[i][j] = rand()%30+30;
        }
    }
    
    for (int i = 0; i < ROW; i++) {
        for (int j = 0; j < COL; j++) {
            cout << matrix[i][j] << "\t";
        }
        cout << endl;
    }
}

void findMaxMin() {
    int maxElement = matrix[0][0];
    int minElement = matrix[0][0];
    for(int i = 0; i < ROW; i++) {
        for(int j = 0; j < COL; j++) {
            if(maxElement < matrix[i][j]) {
                maxElement = matrix[i][j];
            }
            if(minElement > matrix[i][j]) {
                minElement = matrix[i][j];
            }
        }
    }
    cout << "Max : " << maxElement << "\n" << "Min : " << minElement << "\n";
}

void calculateMinMax() {
    generateArray();
    findMaxMin();
}

int main()
{
    //numbersCheck();
    //yearCheck();
    //squareSolution();
    //checkEven();
    //squares();
    //getMax();
    //stepFunction();
    //calculateMinMax();
    return 0;
}