#include <iostream>
using namespace std;

int main()
{
	int TOTAL = 3;
	
	struct TRAIN
	{
		char destination[255];
		int number;
		char time[6];
	};
	
	TRAIN Trains[TOTAL];
	
	for(int i=0; i<TOTAL; i++)
	{
	    cout << "[*] Destination: ";
		cin >> Trains[i].destination;
		cout << "[*] Train no: ";
		cin >> Trains[i].number;
		cout << "[*] Departure time: ";
		cin >> Trains[i].time;
	}

	 for (int i = 0; i < TOTAL - 1; i++)
        for (int j = i + 1; j < TOTAL; j++)
			if (Trains[i].number > Trains[j].number) {
                TRAIN tmp = Trains[i];
                Trains[i] = Trains[j];
                Trains[j] = tmp;
            }

    int currentNumber = 0;
	bool isFound = false;
	cout << "\n[*] Search by train NO: ";
	cin >> currentNumber;
	for(int i=0; i<TOTAL; i++)
	{
		if(Trains[i].number == currentNumber)
		{
			cout << "Train No: " << Trains[i].number << " | City: " << Trains[i].destination << " | Time: " << Trains[i].time << "\n";
			isFound = true;
		}
	}
	if(!isFound)
	{
		cout << "\nNo trains\n";
	}
	return 0;
}