#include <iostream>

using namespace std;

struct STUDENT
{
    char NAME [50];
    int GROUP;
    int MARKS [5];
};

int main()
{
    const int ARR_SIZE = 5;
    STUDENT Student[ARR_SIZE];  
    for(int i = 0; i < ARR_SIZE; i++)
    {
        cout << "[*] Data of " << i + 1 << " student:\n";
        cout << "-> Enter the lastname, name: ";
        cin.getline(Student[i].NAME, 50);
        cout << "-> Group #: ";
        cin >> Student[i].GROUP;
        cout << "-> Marks (1..5):\n";
        for(int j = 0; j < 5; ++j)
            cin >> Student[i].MARKS[j];
        cin.get();
    }
    int idx;
    STUDENT tmp, min;
    for(int i = 0; i < ARR_SIZE; ++i)
    {
        min = Student[i];
        idx = i;
        for(int j = i; j < ARR_SIZE; ++j)
        {
            if(min.GROUP >= Student[j].GROUP)
            {
                min = Student[j];
                idx = j;
            }
        }
        tmp = Student[i];
        Student[i] = min;
        Student[idx] = tmp;
    }
    
    double avg = 0;
    int count = 0;
    for(int i = 0; i < ARR_SIZE; ++i)
    {
        for(int j = 0; j < 5; ++j)
        {
            avg += Student[i].MARKS[j];
            //cout << "AVG: " << avg << " | ";
        }
        avg /= 5;
        if(avg >= 4.0)
        {
            count++;
            cout << Student[i].NAME << " | Group: " << Student[i].GROUP <<'\n';
        }
        avg = 0;
    }
    
    if(count == 0) {
        cout << "No such students\n";
    }
 
    return 0;
}