oList = ["[", "{", "("]
cList = ["]", "}", ")"]

text = input("Input text: ")

stack = []
for i in text:
    if i in oList:
        stack.append(i)
    elif i in cList:
        position = cList.index(i)
        if ((len(stack) > 0) and
                (oList[position] == stack[len(stack) - 1])):
            stack.pop()
        else:
            print("False")
            exit()

if len(stack) == 0:
    print("True")
else:
    print("False")
