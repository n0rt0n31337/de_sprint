import random
import time
from requests_tor import RequestsTor
from bs4 import BeautifulSoup
import json


class bcolors:
    HEADER = '\033[95m'
    OKBLUE = '\033[94m'
    OKCYAN = '\033[96m'
    OKGREEN = '\033[92m'
    WARNING = '\033[93m'
    FAIL = '\033[91m'
    ENDC = '\033[0m'
    BOLD = '\033[1m'
    UNDERLINE = '\033[4m'


TOTAL_PAGES = 40

data = {
    "data": []
}

req = RequestsTor(tor_ports=(9050,), tor_cport=9051)
page = 0

while page < TOTAL_PAGES:
    print(f"{bcolors.OKBLUE}[*]{bcolors.ENDC} Parsing page {page} from {TOTAL_PAGES} ...")
    url = "https://hh.ru/search/vacancy?text=python+разработчик&salary=&page=0&clusters=true&ored_clusters=true" \
          "&enable_snippets=true "
    headers = {'User-Agent': 'Mozilla/5.0', 'Host': 'voronezh.hh.ru'}
    time.sleep(random.randint(1, 7))
    try:
        response = req.get(url, headers=headers)
        if response.status_code == 200:
            page_entity = BeautifulSoup(response.text, "lxml")
            vacancy_items = page_entity.find_all("div", class_="serp-item")
            i = 0
            while i < len(vacancy_items):
                current_vacancy = {"title": "", "work experience": "", "salary": "", "region": ""}
                href = vacancy_items[i].find("a", class_="serp-item__title")['href']
                current_vacancy["title"] = vacancy_items[i].find("a", class_="serp-item__title").text
                print(f"{bcolors.OKBLUE}[*]{bcolors.ENDC} Parsing vacancy {i} ({current_vacancy['title']}) on page {page}...")
                region = vacancy_items[i].find("div", attrs={"data-qa": "vacancy-serp__vacancy-address"})
                if region is not None:
                    current_vacancy["region"] = region.text.split(',', 1)[0]

                sleep_duration = random.randint(1, 7)
                print(f"{bcolors.WARNING}[*]{bcolors.ENDC} Sleeping {sleep_duration} before request...")
                time.sleep(sleep_duration)

                try:
                    print(f"{bcolors.OKBLUE}[*]{bcolors.ENDC} Performing request ...")
                    response = req.get(href, headers=headers)
                    print(f"{bcolors.OKGREEN}[+]{bcolors.ENDC} Success!")
                    print(f"{bcolors.OKBLUE}[*]{bcolors.ENDC} Parsing rest and dumping to file...")
                    vacancy_entity = BeautifulSoup(response.text, "lxml")

                    exp = vacancy_entity.find(attrs={"data-qa": "vacancy-experience"})
                    if exp is not None:
                        current_vacancy["work experience"] = exp.text

                    salary_div = vacancy_entity.find("div", attrs={"data-qa": "vacancy-salary"})
                    if salary_div is not None:
                        salary_span = salary_div.find("span")
                        if salary_span is not None:
                            current_vacancy["salary"] = salary_span.text

                    data['data'].append(current_vacancy)

                    with open("data.json", "w") as file:
                        json.dump(data, file, ensure_ascii=False)
                    i += 1
                    print(f"{bcolors.OKGREEN}[+]{bcolors.ENDC} Done")
                except:
                    print(
                        f"{bcolors.FAIL}[-]{bcolors.ENDC} Whoops... Response code is: {response.status_code}. Response headers: {response.headers}")
            page += 1
    except:
        print(f"{bcolors.FAIL}[-]{bcolors.ENDC} Whoops... Response code is: {response.status_code}. Response headers: {response.headers}")
        print(f"{bcolors.OKGREEN}[+]{bcolors.ENDC} Page was successful parsed.")
